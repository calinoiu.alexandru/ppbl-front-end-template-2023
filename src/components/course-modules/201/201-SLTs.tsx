import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Divider, Heading, Box } from "@chakra-ui/react";
import React from "react";
import Introduction from "@/src/components/course-modules/201/Introduction.mdx";
import LessonNavigation from "@/src/components/lms/Lesson/LessonNavigation";


const SLTs201 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <SLTsItems moduleTitle="Module 201" moduleNumber={201} />
      <Divider mt="5" />
      <Box py="5">
        <Introduction />
      </Box>
      <LessonNavigation moduleNumber={201} currentSlug="slts" />
    </Box>
  );
};

export default SLTs201;