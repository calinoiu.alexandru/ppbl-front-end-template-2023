import { Flex, Box, Heading, Text } from "@chakra-ui/react";
import * as React from "react";
import { StatusBox } from "@/src/components/lms/Status/StatusBox";
import { useAddress, useAssets } from "@meshsdk/react";
import { NativeScript, resolveNativeScriptHash, resolvePaymentKeyHash } from "@meshsdk/core";
import { TX_FROM_ADDRESS_WITH_POLICYID } from "../102/queries";
import { useLazyQuery } from "@apollo/client";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { PPBLContext } from "@/src/context/PPBLContext";

type Props = {
  children?: React.ReactNode;
};

const Status202: React.FC<Props> = ({ children }) => {
  const walletAssets = useAssets();
  const address = useAddress(0);

  const [cliTokenPresent, setCliTokenPresent] = React.useState(false);

  const [meshTokenPresent, setMeshTokenPresent] = React.useState(false);

  const [plutusTxTokenPresent, setPlutusTxTokenPresent] = React.useState(false);
  const plutusTxTokenPolicyId = "b4dff8a4bf58ef312cfc498231d4385349cdf9bc39e3bd0278f7637e"

  const [aikenTokenPresent, setAikenTokenPresent] = React.useState(false);
  const aikenTokenPolicyId = "4c7e95b2ddab220434e90dcf3f504e824a663b48a888aa59b7429e77"

  const ppblContext = React.useContext(PPBLContext);

  React.useEffect(() => {
    // Module 202.2: Native Script CLI
    if (ppblContext.cliAddress && ppblContext.cliAddress !== "" && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(ppblContext.cliAddress);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setCliTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
    }

    // Module 202.3: Mesh
    if (address && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(address);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setMeshTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
      setPlutusTxTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == plutusTxTokenPolicyId));
      setAikenTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == aikenTokenPolicyId));
    }


  }, [walletAssets, address, ppblContext.cliAddress]);

  return (
    <>
      <Flex direction="row" justifyContent="center" alignItems="center">
        <StatusBox condition={false} text="202.1: Mint with GameChanger" />
        <StatusBox condition={cliTokenPresent} text="202.2: Mint with Native Script" />
        <StatusBox condition={meshTokenPresent} text="202.3: Mint with Mesh" />
        <StatusBox condition={plutusTxTokenPresent} text="202.4: Mint with PlutusTx" />
        <StatusBox condition={aikenTokenPresent} text="202.5: Mint with Aiken" />
      </Flex>
    </>
  );
};

export default Status202;
