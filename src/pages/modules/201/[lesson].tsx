import SLTs201 from "@/src/components/course-modules/201/201-SLTs";
import Lesson2011 from "@/src/components/course-modules/201/Lesson-2011";
import Lesson2012 from "@/src/components/course-modules/201/Lesson-2012";
import Lesson2013 from "@/src/components/course-modules/201/Lesson-2013";
import Lesson2014 from "@/src/components/course-modules/201/Lesson-2014";
import Lesson2015 from "@/src/components/course-modules/201/Lesson-2015";
import ComingSoon from "@/src/components/lms/Lesson/ComingSoon";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json"
import Summary201 from "@/src/components/course-modules/201/Summary201";
import Commit201 from "@/src/components/course-modules/201/Commit201";

const Module201Lessons = () => {

  const moduleSelected = slt.modules.find((m) => m.number === 201);

  const status = null

  // Sidebar items are generated from module.lessons i.e. from the JSON file
  // Here we simply set the contents by matching the slug and key
  const lessons = [
    { key:"slts", component:<><SLTs201 /></>},
    { key:"2011", component:<Lesson2011 />},
    { key:"2012", component:<Lesson2012 />},
    { key:"2013", component:<Lesson2013 />},
    { key:"2014", component:<Lesson2014 />},
    { key:"2015", component:<Lesson2015 />},
    { key:"commit", component:<Commit201 />},
    { key:"summary", component:<Summary201 />},
  ]

  return (
    <ModuleLessons items={moduleSelected?.lessons ?? []} modulePath="/modules/201" selected={0} lessons={lessons} status={status}/>
  )

};

export default Module201Lessons;