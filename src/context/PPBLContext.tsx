import { useAddress, useWallet } from "@meshsdk/react";
import { createContext, useEffect, useState } from "react";
import { useLazyQuery, useQuery } from "@apollo/client";
import { Heading, Center, Spinner } from "@chakra-ui/react";

import { issuerPolicyID, treasury } from "gpte-config";
import { GraphQLUTxO } from "@/src/types/cardanoGraphQL";
import { TREASURY_UTXO_QUERY } from "@/src/data/queries/treasuryQueries";
import { Asset } from "@meshsdk/core";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { hexToString } from "@/src/utils";
import { ContributorReferenceDatum } from "@/src/types/contributor";
import { getInlineDatumForContributorReference } from "../data/queries/getInlineDatumForContributorReference";
import { TX_FROM_ADDRESS_WITH_POLICYID } from "../components/course-modules/102/queries";

// Add Contributor Reference Datum
// Add CLI Address

interface CurrentPPBLContext {
  connectedContribToken: string | undefined;
  contributorReferenceDatum: ContributorReferenceDatum | undefined;
  cliAddress: string | undefined;
  connectedIssuerToken: string | undefined;
  treasuryUTxO: GraphQLUTxO | undefined;
  loading: boolean;
  error: boolean;
}

const initialContext: CurrentPPBLContext = {
  connectedContribToken: undefined,
  contributorReferenceDatum: undefined,
  cliAddress: undefined,
  connectedIssuerToken: undefined,
  treasuryUTxO: undefined,
  loading: false,
  error: false,
};

const PPBLContext = createContext<CurrentPPBLContext>(initialContext);

type Props = {
  children?: React.ReactNode;
};

const PPBLContextProvider: React.FC<Props> = ({ children }) => {
  const address = useAddress();

  const [currentContext, setCurrentContext] = useState<CurrentPPBLContext>(initialContext);
  const [currentTreasuryUTxO, setCurrentTreasuryUTxO] = useState<GraphQLUTxO | undefined>(undefined);

  const [connectedContributorToken, setConnectedContributorToken] = useState<Asset | undefined>(undefined);
  const [contribTokenName, setContribTokenName] = useState<string | undefined>(undefined);

  const [connectedReferenceDatum, setConnectedReferenceDatum] = useState<ContributorReferenceDatum | undefined>(
    undefined
    );

  // Refactor by moving all queries to /src/data - currently this query is in Module 102
  const [getQuery1, { data: dataQuery1 }] = useLazyQuery(TX_FROM_ADDRESS_WITH_POLICYID);
  const [connectedCLIAddress, setConnectedCLIAddress] = useState<string | undefined>(undefined);

  const [connectedIssuerToken, setConnectedIssuerToken] = useState<Asset | undefined>(undefined);
  const [issuerTokenName, setIssuerTokenName] = useState<string | undefined>(undefined);

  const { connected, wallet } = useWallet();

  const { data, loading, error } = useQuery(TREASURY_UTXO_QUERY, {
    variables: {
      contractAddress: treasury.address,
    },
  });

  useEffect(() => {
    const newContext: CurrentPPBLContext = {
      connectedContribToken: contribTokenName,
      contributorReferenceDatum: connectedReferenceDatum,
      cliAddress: connectedCLIAddress,
      connectedIssuerToken: issuerTokenName,
      treasuryUTxO: currentTreasuryUTxO,
      loading: loading,
      error: error != undefined,
    };
    setCurrentContext(newContext);
  }, [contribTokenName, connectedReferenceDatum, issuerTokenName, currentTreasuryUTxO, loading, error]);

  useEffect(() => {
    if (data && data.utxos.length == 1) {
      setCurrentTreasuryUTxO(data.utxos[0]);
    }
  }, [data]);

  useEffect(() => {
    const fetchContributorToken = async () => {
      const _token = await wallet.getPolicyIdAssets(contributorTokenPolicyId);
      if (_token.length > 0) {
        setConnectedContributorToken(_token[0]);
      }
    };

    const fetchIssuerToken = async () => {
      const _token = await wallet.getPolicyIdAssets(issuerPolicyID);
      if (_token.length > 0) {
        setConnectedIssuerToken(_token[0]);
      }
    };

    if (connected) {
      fetchContributorToken();
      fetchIssuerToken();
    }
  }, [connected]);

  useEffect(() => {
    const fetchContributorReferenceDatum = async () => {
      if (connectedContributorToken) {
        const _hexName = connectedContributorToken.unit.substring(62);
        const _tokenName = hexToString(_hexName);

        const _datum = await getInlineDatumForContributorReference(_tokenName);
        console.log("Context:", _datum);

        const _formatted_datum: ContributorReferenceDatum = {
          fields: [{ int: _datum.fields[0].int }, { list: _datum.fields[1].list }],
          constructor: 0,
        };

        setConnectedReferenceDatum(_formatted_datum);
        setContribTokenName(_tokenName);
      }
    };

    if (connectedContributorToken) {
      fetchContributorReferenceDatum();
    }
  }, [connectedContributorToken]);

  useEffect(() => {
    const fetchIssuerTokenName = async () => {
      if (connectedIssuerToken) {
        const _hexName = connectedIssuerToken.unit.substring(56);
        const _tokenName = hexToString(_hexName);
        setIssuerTokenName(_tokenName);
      }
    };

    if (connectedIssuerToken) {
      fetchIssuerTokenName();
    }
  }, [connectedIssuerToken]);

  useEffect(() => {
    if (address) {
      getQuery1({
        variables: {
          browserWalletAddress: address,
          tokenPolicyId: contributorTokenPolicyId,
        },
      });
    }
  }, [address, getQuery1]);

  useEffect(() => {
    if (dataQuery1 && dataQuery1.transactions.length > 0) {
      // we may want to order the transactions from most recent - there are options here
      // After this works, refactor with Types
      const _contributorOutput = dataQuery1.transactions
        .find((tx: any) =>
          tx.outputs.some(
            (output: any) => output.address !== address && output.tokens[0]?.asset.policyId == contributorTokenPolicyId
          )
        )
        .outputs.find((output: any) => output.tokens[0]?.asset.policyId == contributorTokenPolicyId);
      if (_contributorOutput) {
        const _cliAddress = _contributorOutput.address;
        setConnectedCLIAddress(_cliAddress);
      }
    }
  }, [dataQuery1]);

  return <PPBLContext.Provider value={currentContext}>{children}</PPBLContext.Provider>;
};

export { PPBLContext, PPBLContextProvider };
